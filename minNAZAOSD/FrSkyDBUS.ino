#include <TinySerial.h>
#include "FrSkyDBUS.h"

/*
1. Course
5e    14 2c 00
5e    1c 03 00

2. Latt
5e    13 38 0c
5e    1b c9 06
5e    23 4e 00

3. Long
5e    12 ef 2e
5e    1a 98 26
5e    22 45 00

4. Speed
5e    11 02 00
5e    19 93 00


5. Alt
5e    01 18 00
5e    09 05 00

6. Voltage
5e    3a 0a 00
5e    3b 05 00
 */

TinySerial FrSkyDBUS(DBUS_PIN, true);

void write_DBUS8(uint8_t Data)
{
  FrSkyDBUS.write(Data);
}

void DBUS_stuff(uint8_t Data)
{
  if (Data == 0x5E) {
    write_DBUS8(0x5D);
    write_DBUS8(0x3E);
  }
  else if (Data == 0x5D) {
    write_DBUS8(0x5D);
    write_DBUS8(0x3D);
  }
  else {
    write_DBUS8(Data);
  }
}

void write_DBUS16(uint16_t Data)
{
  uint8_t Data_send;
  Data_send = Data;
  DBUS_stuff(Data_send);
  Data_send = Data >> 8 & 0xff;
  DBUS_stuff(Data_send);
}

static void sendDataHead(uint8_t Data_id)
{
  write_DBUS8(DBUS_Header);
  write_DBUS8(Data_id);
}

static void sendDataTail(void)
{
  write_DBUS8(DBUS_Tail);
}

void inline dbus_send_rpm(uint16_t rpm) //Not used here. For debug only
{
  sendDataHead(ID_RPM);
  write_DBUS16(rpm);
}

void inline dbus_send_voltage(void)
{
  float voltage = osd_vbat_A * 0.5238;
  uint16_t voltage_bp = (uint16_t) (voltage);
  uint16_t voltage_ap = (uint16_t) ((voltage - (uint16_t)voltage) * 10.0f);
  sendDataHead(ID_Voltage_Amp_bp);
  write_DBUS16(voltage_bp);
  sendDataHead(ID_Voltage_Amp_ap);
  write_DBUS16(voltage_ap);
}

void inline send_course(void)
{
  uint16_t course_bp = (uint16_t) osd_heading;
  uint16_t course_ap = 0; /* I don't need after point value */
  sendDataHead(ID_Course_bp);
  write_DBUS16(course_bp);
/*  sendDataHead(ID_Course_ap);
  write_DBUS16(course_ap);*/
}

void inline send_gps_speed(void)
{
  uint16_t speed_bp = (uint16_t) osd_groundspeed;
  uint16_t speed_ap = 0;
  sendDataHead(ID_GPS_speed_bp);
  write_DBUS16(speed_bp);
/*  sendDataHead(ID_GPS_speed_ap);
  write_DBUS16(speed_ap);*/
}

void inline send_gps_altitude(void)
{
  uint16_t alt_bp = (uint16_t) osd_alt;
  uint16_t alt_ap = 0;
  sendDataHead(ID_GPS_Altitude_bp);
  write_DBUS16(alt_bp);
/*  sendDataHead(ID_GPS_Altitude_ap);
  write_DBUS16(alt_ap);*/
}

void inline send_gps_latitude(void)
{
  uint16_t latitude_bp = 0;
  uint16_t latitude_ap = 0;
  uint16_t n_s = 0;
  float lat;
  if (osd_lat < 0)
  {
    n_s = 'S';
    lat = osd_lat * (-1);
  } else
  {
    n_s = 'N';
    lat = osd_lat;
  }
  latitude_bp = (uint16_t) lat;
  latitude_ap = (uint16_t)((lat - (uint16_t) lat) * 10000);
  sendDataHead(ID_Latitude_bp);
  write_DBUS16(latitude_bp);
  sendDataHead(ID_Latitude_ap);
  write_DBUS16(latitude_ap);
  sendDataHead(ID_N_S);
  write_DBUS16(n_s);
}

void inline send_gps_longitude(void)
{
  uint16_t longitude_bp = 0;
  uint16_t longitude_ap = 0;
  uint16_t e_w = 0;
  float lon;
  if (osd_lon < 0)
  {
    e_w = 'W';
    lon = osd_lon * (-1);
  } else
  {
    e_w = 'E';
    lon = osd_lon;
  }
  longitude_bp = (uint16_t) lon;
  longitude_ap = (uint16_t)((lon - (uint16_t) lon) * 10000);
  sendDataHead(ID_Longitude_bp);
  write_DBUS16(longitude_bp);
  sendDataHead(ID_Longitude_ap);
  write_DBUS16(longitude_ap);
  sendDataHead(ID_E_W);
  write_DBUS16(e_w);
}

void dbus_send_cycle(int cnt)
{
  if (osd_fix_type > 1) /* 2D or 3D fix */
  {
    switch (cnt)
    {
    case 1:
      dbus_send_voltage();
      sendDataTail();
      break;
    case 2:
      send_course();
      sendDataTail();
      break;
    case 3:
      send_gps_speed();
      sendDataTail();
      break;
    case 4:
      send_gps_altitude();
      sendDataTail();
      break;
    case 5:
      send_gps_latitude();
      sendDataTail();
      break;
    case 6:
      send_gps_longitude();
      sendDataTail();
      break;
    }
  } else
  {
    dbus_send_voltage();
    sendDataTail();
  }
}

void dbus_setup() {
  FrSkyDBUS.begin(DBUS_BAUD);
}
